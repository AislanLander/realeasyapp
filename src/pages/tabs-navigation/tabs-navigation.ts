import { CalculadorasPage } from './../calculadoras/calculadoras';
import { EducacaoPage } from './../educacao/educacao';
import { Component } from '@angular/core';

import { ListingPage } from '../listing/listing';
import { ProfilePage } from '../profile/profile';
import { NotificationsPage } from '../notifications/notifications';
import { GastosPage } from '../gastos/gastos';

@Component({
  selector: 'tabs-navigation',
  templateUrl: 'tabs-navigation.html'
})
export class TabsNavigationPage {
  tab1Root: any;
  tab2Root: any;
  tab3Root: any;
  tab4Root: any;
  tab5Root: any;
  constructor() {
    this.tab1Root = GastosPage;
    this.tab2Root = ProfilePage;
    this.tab3Root = GastosPage;
    this.tab4Root = EducacaoPage;
    this.tab5Root = CalculadorasPage;
  }
}
