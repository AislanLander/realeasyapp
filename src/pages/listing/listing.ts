import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage } from 'ionic-angular';

import { FeedPage } from '../feed/feed';
import 'rxjs/Rx';

import { ListingModel } from './listing.model';
import { ListingService } from './listing.service';


@Component({
  selector: 'listing-page',
  templateUrl: 'listing.html',
})
export class ListingPage {
  listing: ListingModel = new ListingModel();
  loading: any;
  rootNavCtrl: NavController;
  
  constructor(
    public nav: NavController,
    public listingService: ListingService,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }


  ionViewDidLoad() {
    this.loading.present();
    this.listingService
      .getData()
      .then(data => {
        this.loading.dismiss();
      });
  }


  goToFeed(category: any) {
    console.log("Clicked goToFeed", category);
    this.nav.push(FeedPage, { category: category });
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }



  public barChartOptions:any = {
    //Código para exibir os valores de cada barra constantemente.
/*     animation: {
      "duration": 1,
      "onComplete": function() {
        var chartInstance = this.chart,
          ctx = chartInstance.ctx;

        ctx.textAlign = 'center';
        ctx.textBaseline = 'bottom';

        this.data.datasets.forEach(function(dataset, i) {
          var meta = chartInstance.controller.getDatasetMeta(i);
          meta.data.forEach(function(bar, index) {
            var data = dataset.data[index];
            ctx.fillText(data, bar._model.x, bar._model.y - 5);
          });
        });
      }
    }, 
    hover: {
      "animationDuration": 2
    }, */
    title:{
      display:false,
      text:"Lançamentos"
  },
  tooltips: {
      mode: 'index',
      intersect: false
  },
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [{
          stacked: true,
          gridLines: {
            display:false
        },
        display: false
      }],
      yAxes: [{
          stacked: true,
          gridLines: {
            display:false
        },
        display: false
      }]
  },
  legend: {
    labels: {
        usePointStyle: true
    },
    position: 'bottom'
},
maintainAspectRatio: true
  };
  public barChartLabels:string[] = ['Orçado', 'Realizado'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  
  public barChartData:any[] = [{
    label: 'Investimento',
        data: [
        '100',
        '200'
    ]
  }, {
    label: 'Gastos',
    data: [
        '100',
        '230'
    ]
}, {
    label: 'Dívidas',
    data: [
        '133',
       '321'
    ]
}];
  public randomizeBar():void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }
  public barChartColors:Array<any> = [
    { // azul
      backgroundColor: '#339aed',
      borderColor: '#339aed',
      pointBackgroundColor: '#339aed',
      pointBorderColor: '#339aed',
      pointHoverBackgroundColor: '#339aed',
      pointHoverBorderColor: '#339aed'
    },
    { // amarelo
      backgroundColor: '#fbbf41',
      borderColor: '#fbbf41',
      pointBackgroundColor: '#fbbf41',
      pointBorderColor: '#fbbf41',
      pointHoverBackgroundColor: '#fbbf41',
      pointHoverBorderColor: '#fbbf41'
    },
    { // vermelho
      backgroundColor: '#fc5830',
      borderColor: '#fc5830',
      pointBackgroundColor: '#fc5830',
      pointBorderColor: '#fc5830',
      pointHoverBackgroundColor: '#fc5830',
      pointHoverBorderColor: '#fc5830'
    }
  ];
}
