import { ListingPage } from './../listing/listing';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SuperTabsController } from 'ionic2-super-tabs';
import { SuperTabs } from 'ionic2-super-tabs';
import { CategoriasPage } from '../categorias/categorias';
import { UserModel } from '../profile/profile.model';

import 'rxjs/Rx';
import { FollowersPage } from '../followers/followers';

@Component({
  selector: 'page-gastos',
  templateUrl: 'gastos.html',
})
export class GastosPage {
  @ViewChild(SuperTabs) superTabs: SuperTabs;  
  page1: any = ListingPage;
  page2: any = CategoriasPage ;

  showIcons: boolean = true;
  showTitles: boolean = true;
  pageTitle: string = 'Full Height';
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private superTabsCtrl : SuperTabsController) {
    const type = navParams.get('type');
    switch (type) {
      case 'icons-only':
        this.showTitles = false;
        this.pageTitle += ' - Icons only';
        break;

      case 'titles-only':
        this.showIcons = false;
        this.pageTitle += ' - Titles only';
        break;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GastosPage');
  }
  ngAfterViewInit() {
        
    }

  hideToolbar() {
    this.superTabsCtrl.showToolbar(false);
  }
  
  showToolbar() {
    this.superTabsCtrl.showToolbar(true);
  }
  
  onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
  }
  
}
