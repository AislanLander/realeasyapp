import { ContactCardPage } from './../contact-card/contact-card';
import { CategoriasService } from './categorias.service';
import { Component } from '@angular/core';
import { MenuController, NavParams, NavController, ModalController } from 'ionic-angular';
import { UserModel } from '../profile/profile.model';
import { ProfileModel } from '../profile/profile.model';
import { AlertController } from 'ionic-angular';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {
  list: Array<UserModel> = [];
  pushPage: any;
  constructor(public ngProgress: NgProgress,public modalCtrl: ModalController, public alertCtrl: AlertController, public categoriasService: CategoriasService,public menu: MenuController, public navParams: NavParams)
  {
    this.pushPage = '';
    this.list = navParams.get('list');
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad CategoriasPage');
    this.categoriasService
    .getData()
    .then(data => {
      this.list = data.followers;

    });
    this.list = this.navParams.get('list');
    this.ngProgress.start();
  }
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Criar nova Categoria?',
      subTitle: 'Escolha o tipo de categoria que deseja adicionar',
      inputs: [
        {
          type: 'radio',
          value: 'recebimentos',
          label: 'Recebimentos',
          checked: true
        },
        {
          type: 'radio',
          value: 'adulto',
          label: 'Adulto',
        },
        {
          type: 'radio',
          value: 'filhos',
          label: 'Filhos',
        },
        {
          type: 'radio',
          value: 'alimentacao',
          label: 'Alimentação',
        },
        {
          type: 'radio',
          value: 'vidaSaude',
          label: 'Vida e saúde',
        },
        {
          type: 'radio',
          value: 'transporte',
          label: 'Transporte',
        },
        {
          type: 'radio',
          value: 'lazerOpcionais',
          label: 'Lazer e Opcionais',
        },
        {
          type: 'radio',
          value: 'moradia',
          label: 'Moradia',
        },
        {
          type: 'radio',
          value: 'dividas',
          label: 'Dívidas',
        },
        {
          type: 'radio',
          value: 'sonhos',
          label: 'Sonhos',
        },
        {
          type: 'radio',
          value: 'meuNegocio',
          label: 'Meu negócio',
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('cancelar clicado');
          }
        },
        {
          text: 'Salvar',
          handler: data => {
            console.log('salvar clicado: ' + JSON.stringify(data));
            console.log(data);
          }
        }
      ]
    });
    alert.present();
  }
  presentProfileModal() {
    let profileModal = this.modalCtrl.create( { userId: 8675309 });
    profileModal.present();
  }

}



