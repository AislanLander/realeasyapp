import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, AlertController, SegmentButton } from 'ionic-angular';
import { Component } from '@angular/core';
import { counterRangeValidator } from '../../components/counter-input/counter-input';

@Component({
  selector: 'page-novo-sonho',
  templateUrl: 'novo-sonho.html',
})
export class NovoSonhoPage {
  section: string;

  post_form: any;
  event_form: FormGroup;
  card_form: FormGroup;

  categories_checkbox_open: boolean;
  categories_checkbox_result;
  toggle = false;
  constructor(public nav: NavController, public alertCtrl: AlertController) {


    this.post_form = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl(''),
      valor: new FormControl(''),
      realizado: new FormControl('', Validators.required),
      inicio: new FormControl(''),
      env_dinheiro: new FormControl(false, Validators.required),
      qntTem: new FormControl('')
    });
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }

  createPost(){
    console.log(this.post_form.value);
  }

  createEvent(){
    console.log(this.event_form.value);
  }

  createCard(){
    console.log(this.card_form.value);
  }

  chooseCategory(){
    let alert = this.alertCtrl.create({
      cssClass: 'category-prompt'
    });
    alert.setTitle('Category');

    alert.addInput({
      type: 'checkbox',
      label: 'Alderaan',
      value: 'value1',
      checked: true
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Bespin',
      value: 'value2'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'Confirm',
      handler: data => {
        console.log('Checkbox data:', data);
        this.categories_checkbox_open = false;
        this.categories_checkbox_result = data;
      }
    });
    alert.present().then(() => {
      this.categories_checkbox_open = true;
    });
  }

}
