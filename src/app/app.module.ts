import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, Provider } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { Providers } from '../shared/providers';
import { Modules } from '../shared/modules';
import { Pages } from '../shared/pages';
import { Components } from '../shared/components';
import { Directives } from '../shared/directives';

@NgModule({
  declarations: [
    MyApp,
    ...Pages,
    ...Components,
    ...Directives
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ...Modules
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ...Pages,
    ...Components
  ],
  providers: [
    ...Providers
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
