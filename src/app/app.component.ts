import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { FormsPage } from '../pages/forms/forms';
import { LayoutsPage } from '../pages/layouts/layouts';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { SettingsPage } from '../pages/settings/settings';
import { FunctionalitiesPage } from '../pages/functionalities/functionalities';

import { IonMaterialSidemenuOptions } from '../components/ion-material-sidemenu';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  // make WalkthroughPage the root (or first) page
  rootPage: any = WalkthroughPage;
  // rootPage: any = TabsNavigationPage;


  pages: Array<{title: string, icon: string, component: any}>;
  pushPages: Array<{title: string, icon: string, component: any}>;
  menuOptions: IonMaterialSidemenuOptions;
  constructor(
    platform: Platform,
    public menu: MenuController,
    public app: App,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar
  ) {
    this.menuOptions = this.menu2(); 
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.splashScreen.hide();
      this.statusBar.styleDefault();
    });

    this.pages = [
      { title: 'Home', icon: 'home', component: TabsNavigationPage },
      { title: 'Forms', icon: 'create', component: FormsPage },
      { title: 'Functionalities', icon: 'code', component: FunctionalitiesPage }
    ];

    this.pushPages = [
      { title: 'Layouts', icon: 'grid', component: LayoutsPage },
      { title: 'Settings', icon: 'settings', component: SettingsPage }
    ];
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.app.getRootNav().push(page.component);
  }

  menu2() {
    let _t = this;
    return {
      header: {
        background: '#ccc url(./assets/menu_bg1.jpg) no-repeat top left / cover',
        picture: 'https://avatars2.githubusercontent.com/u/1867703?v=3&s=460',
        username: 'Roberto Cesar Augusto',
        email: 'roberto@gmail.com',
        onClick: () => { alert('Header clicado'); }
      },
      entries: [
        { isSelected: true, title: 'Orçamento', leftIcon: 'icon-money', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: 'Sonhos', leftIcon: 'icon-target', onClick: (entry) => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: '', isDivider: true },
        { title: 'Perfil', leftIcon: 'md-person', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: 'Família', leftIcon: 'icon-family-silhouette', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: 'Patrimônio', leftIcon: 'icon-money-security-save-shield', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: 'Gastos', leftIcon: 'icon-coins', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: 'Despesas', leftIcon: 'ios-basket-outline', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { title: 'Histórico e Relatórios', leftIcon: 'md-paper', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } },
        { isDivider: true },
        { title: 'configurações', leftIcon: 'settings', onClick: () => { _t.nav.setRoot(SettingsPage) } },
        { title: 'ajuda', leftIcon: 'help-circle', onClick: () => { _t.nav.setRoot(TabsNavigationPage) } }
      ]
    };
  }
}
