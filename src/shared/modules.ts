import { ChartsModule } from 'ng2-charts';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { NgProgressModule } from 'ngx-progressbar';

export const Modules = [
  SuperTabsModule.forRoot(),
  ChartsModule,
  NgProgressModule
];
