import { NovoSonhoPage } from './../pages/novo-sonho/novo-sonho';
import { ItensPage } from './../pages/itens/itens';
import { InvestimentosPage } from './../pages/investimentos/investimentos';
import { LancamentoPage } from './../pages/lancamento/lancamento';
import { ListingPage } from '../pages/listing/listing';
import { FeedPage } from '../pages/feed/feed';
import { FollowersPage } from '../pages/followers/followers';
import { LayoutsPage } from '../pages/layouts/layouts';
import { FormsPage } from '../pages/forms/forms';
import { LoginPage } from '../pages/login/login';
import { NotificationsPage } from '../pages/notifications/notifications';
import { ProfilePage } from '../pages/profile/profile';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { SettingsPage } from '../pages/settings/settings';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { SchedulePage } from '../pages/schedule/schedule';
import { List1Page } from '../pages/list-1/list-1';
import { List2Page } from '../pages/list-2/list-2';
import { GridPage } from '../pages/grid/grid';
import { FormLayoutPage } from '../pages/form-layout/form-layout';
import { FiltersPage } from '../pages/filters/filters';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { FunctionalitiesPage } from '../pages/functionalities/functionalities';
import { MapsPage } from '../pages/maps/maps';
import { FacebookLoginPage } from '../pages/facebook-login/facebook-login';
import { GoogleLoginPage } from '../pages/google-login/google-login';
import { ContactCardPage } from '../pages/contact-card/contact-card';
import { GastosPage } from '../pages/gastos/gastos';
import { CategoriasPage } from '../pages/categorias/categorias';
import { EducacaoPage } from '../pages/educacao/educacao';
import { CalculadorasPage } from '../pages/calculadoras/calculadoras';

export const Pages = [
  NovoSonhoPage,
  ItensPage,
  EducacaoPage,
  LancamentoPage,
  InvestimentosPage,
  CategoriasPage,
  ListingPage,
  FeedPage,
  FollowersPage,
  LayoutsPage,
  FormsPage,
  LoginPage,
  NotificationsPage,
  ProfilePage,
  TabsNavigationPage,
  WalkthroughPage,
  SettingsPage,
  SignupPage,
  ForgotPasswordPage,
  SchedulePage,
  List1Page,
  List2Page,
  GridPage,
  FormLayoutPage,
  FiltersPage,
  TermsOfServicePage,
  PrivacyPolicyPage,
  MapsPage,
  FunctionalitiesPage,
  FacebookLoginPage,
  GoogleLoginPage,
  ContactCardPage,
  GastosPage,
  CalculadorasPage
];
