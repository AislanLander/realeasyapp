import { ColorRadio } from '../components/color-radio/color-radio';
import { ShowHideInput } from '../components/show-hide-password/show-hide-input';

export const Directives = [
  ShowHideInput,
  ColorRadio,
];
