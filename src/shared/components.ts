import { ProgressBarComponent } from './../components/progress-bar/progress-bar';
import { PreloadImage } from '../components/preload-image/preload-image';
import { BackgroundImage } from '../components/background-image/background-image';
import { ShowHideContainer } from '../components/show-hide-password/show-hide-container';
import { CounterInput } from '../components/counter-input/counter-input';
import { Rating } from '../components/rating/rating';
import { GoogleMap } from '../components/google-map/google-map';
import { IonMaterialSidemenu } from '../components/ion-material-sidemenu';

export const Components = [
  ProgressBarComponent,
  IonMaterialSidemenu,
  PreloadImage,
  BackgroundImage,
  ShowHideContainer,
  CounterInput,
  Rating,
  GoogleMap,
  ProgressBarComponent
]
