import { CategoriasService } from './../pages/categorias/categorias.service';
import { FacebookLoginService } from '../pages/facebook-login/facebook-login.service';
import { GoogleLoginService } from '../pages/google-login/google-login.service';
import { GoogleMapsService } from '../pages/maps/maps.service';
import { FeedService } from '../pages/feed/feed.service';
import { ListingService } from '../pages/listing/listing.service';
import { ProfileService } from '../pages/profile/profile.service';
import { NotificationsService } from '../pages/notifications/notifications.service';
import { List1Service } from '../pages/list-1/list-1.service';
import { List2Service } from '../pages/list-2/list-2.service';
import { ScheduleService } from '../pages/schedule/schedule.service';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NativeStorage } from '@ionic-native/native-storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Keyboard } from '@ionic-native/keyboard';
import { Geolocation } from '@ionic-native/geolocation';
import { EmailComposer } from '@ionic-native/email-composer';

export const Providers = [
  CategoriasService,
  FeedService,
  ListingService,
  ProfileService,
  NotificationsService,
  List1Service,
  List2Service,
  ScheduleService,
  FacebookLoginService,
  GoogleLoginService,
  GoogleMapsService,
  SplashScreen,
  StatusBar,
  SocialSharing,
  NativeStorage,
  InAppBrowser,
  CallNumber,
  Facebook,
  GooglePlus,
  Keyboard,
  Geolocation,
  EmailComposer
];
